CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 
 INTRODUCTION
 ------------
 
 This module integrate Toastify js library into drupal site, 
 for displaying the default drupal messages using toastify.js plugin. 
 You can configure your display settings per message type in settings form.
 
 REQUIREMENTS
 ------------
 
 ```php
Jquery colorpicker 8.x-2.x
```

INSTALLATION
------------

using composer
```php
composer require drupal/toastify
```

CONFIGURATION
-------------

```php
/admin/config/user-interface/toastify

/admin/people/permissions#module-toastify
```

MAINTAINERS
-----------

* Szczepan Musial (lamp5)
